package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    PetRock rocky;

    @BeforeEach
    void setUp() {
        rocky = new PetRock("Rocky");
    }

    @Test
    void getName() {
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void testHappy() {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled ("Exception throwing not yet defined")
    @Test
    void nameFail() {
        assertThrows(IllegalStateException.class,
                () -> rocky.getHappyMessage());
    }

    @Test
    void name() {
        rocky.playWithRock();
        String actual = rocky.getHappyMessage();

        assertEquals("I'm happy!", actual);
    }

    @Test
    void testFavNumber() {
        assertEquals(42, rocky.getFavNumber());
    }

    @Test
    void emptyNameFail() {
        assertThrows(IllegalArgumentException.class,
                () -> new PetRock(""));
    }
}